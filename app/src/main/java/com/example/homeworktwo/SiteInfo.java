package com.example.homeworktwo;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

public class SiteInfo implements Serializable {
    private String siteTitle;
    private String siteType;
    private String siteUrl;
    private String email;
    private String category;
    private String profileImage;

    public SiteInfo(String siteTitle, String siteType, String siteUrl, String email, String category, String profileImage) {
        this.siteTitle = siteTitle;
        this.siteType = siteType;
        this.siteUrl = siteUrl;
        this.email = email;
        this.category = category;
        this.profileImage = profileImage;
    }

    public String getSiteTitle() {
        return siteTitle;
    }

    public void setSiteTitle(String siteTitle) {
        this.siteTitle = siteTitle;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Override
    public String toString() {
        return "SiteInfo{" +
                "siteTitle='" + siteTitle + '\'' +
                ", siteType='" + siteType + '\'' +
                ", siteUrl='" + siteUrl + '\'' +
                ", email='" + email + '\'' +
                ", category='" + category + '\'' +
                ", profileImage='" + profileImage + '\'' +
                '}';
    }
}
