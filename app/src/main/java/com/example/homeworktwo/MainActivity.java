package com.example.homeworktwo;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private EditText mEditTextTitle;
    private EditText mEditTextSiteUrl;
    private EditText mEditTextEmail;
    private ImageView mImageViewProfileImage;
    private RadioGroup mRadioGroupSiteType;
    private Spinner mSpinnerCategory;
    private Button mButtonChooseImage;
    private Button mButtonSave;

    private String selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize components
        onComponentsInitialized();

        mButtonChooseImage.setOnClickListener(view ->{
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            activityResultLauncher.launch(intent);
        });

        mButtonSave.setOnClickListener(view -> {
            if(isValidSiteDetail(getSiteInfo())){
                Intent intent = new Intent(MainActivity.this, SiteDetailActivity.class);
                intent.putExtra("siteInfo", getSiteInfo());

                Log.d(MainActivity.class.getSimpleName(), getSiteInfo().toString());

                startActivity(intent);
            }
        });

    }


    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        try {
                            selectedImage = result.getData().getData().toString();

                            Uri selectedImageUri = result.getData().getData();
                             InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            mImageViewProfileImage.setImageBitmap(selectedImage);

                            imageStream.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                }
            }
    );


    private void onComponentsInitialized(){
        mEditTextTitle = findViewById(R.id.edit_text_title);
        mEditTextSiteUrl = findViewById(R.id.edit_text_site_url);
        mEditTextEmail = findViewById(R.id.edit_text_email);

        mRadioGroupSiteType = findViewById(R.id.radio_group_site_type);

        String [] categories = {"Tech", "Health", "Sport"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>
                (this, R.layout.support_simple_spinner_dropdown_item, categories);

        mSpinnerCategory = findViewById(R.id.spinner_category);
        mSpinnerCategory.setAdapter(arrayAdapter);

        mImageViewProfileImage = findViewById(R.id.image_view_profil_image);

        mButtonChooseImage = findViewById(R.id.btn_choose_image);
        mButtonSave = findViewById(R.id.button_save);
    }



    private SiteInfo getSiteInfo() {

        int selectedChildId = mRadioGroupSiteType.getCheckedRadioButtonId();
        RadioButton selectedChild = findViewById(selectedChildId);

        return new SiteInfo(
                mEditTextTitle.getText().toString(),
                selectedChild.getText().toString(),
                mEditTextSiteUrl.getText().toString(),
                mEditTextEmail.getText().toString(),
                mSpinnerCategory.getSelectedItem().toString(),
                selectedImage
        );
    }

    private boolean isValidSiteDetail(SiteInfo siteInfo){

        boolean isValid = true;

        if(siteInfo.getSiteTitle().length() == 0){
            mEditTextTitle.setError("Title is required");
            isValid = false;
        }

        if(siteInfo.getSiteUrl().length() == 0){
            mEditTextSiteUrl.setError("Password is required");
            isValid = false;
        }

        if(siteInfo.getEmail().length() == 0){
            mEditTextEmail.setError("Email is required");
            isValid = false;
        }

        if( siteInfo.getEmail().length() != 0  && !isValidEmail(siteInfo.getEmail())){
            mEditTextEmail.setError("Invalid Email");
            isValid = false;
        }


        if(selectedImage == null){
            Toast.makeText(MainActivity.this, "Please choose Image",Toast.LENGTH_LONG).show();
            isValid = false;
        }

        return  isValid;
    }

    private boolean isValidEmail(String emailStr) {
        String pattern ="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
        return emailStr.matches(pattern);
    }

}