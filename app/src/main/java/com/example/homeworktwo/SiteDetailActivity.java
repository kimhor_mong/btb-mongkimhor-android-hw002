package com.example.homeworktwo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class SiteDetailActivity extends AppCompatActivity {

    TextView mTextViewTitle;
    TextView mTextSiteType;
    TextView mTextViewCategory;
    TextView mTextViewSiteUrl;
    TextView mTextViewEmail;

    ImageView mImageViewProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_detail);

        // initialize Components
        onInitializeComponents();

        // set value for components
        setValueForComponents();
    }

    private void setValueForComponents(){
        Intent intent = getIntent();
        SiteInfo siteInfo = (SiteInfo) intent.getSerializableExtra("siteInfo");

        try {
            InputStream imageStream = getContentResolver().openInputStream(Uri.parse(siteInfo.getProfileImage()));
            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            mImageViewProfileImage.setImageBitmap(selectedImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        mTextViewTitle.setText(siteInfo.getSiteTitle());
        mTextSiteType.setText(siteInfo.getSiteType());
        mTextViewCategory.setText(siteInfo.getCategory());
        mTextViewSiteUrl.setText(siteInfo.getSiteUrl());
        mTextViewEmail.setText(siteInfo.getEmail());

    }

    private void onInitializeComponents(){
         mTextViewTitle = findViewById(R.id.text_view_title);
         mTextSiteType = findViewById(R.id.text_view_site_type);
         mTextViewSiteUrl = findViewById(R.id.text_view_site_url);
         mTextViewCategory = findViewById(R.id.text_view_category);
         mTextViewEmail = findViewById(R.id.text_view_email);
         mImageViewProfileImage = findViewById(R.id.image_view_profil_image);
    }
}